#include <iostream>
using namespace std;

#include "Pila.h"

//FUNCION MAIN
int main () {

int max, resp, opt, dato;
resp = 1;

cout << "Ingrese la dimension del array...";
cin >> max;

Pila p = Pila(max);

//CICLO PRINCIPALE
while (resp == 1){
	cout << "Agregar/push [1]" << endl << "Remover/pop [2]" << endl <<
			   "Ver pila [3]" << endl << "Salir [4]" << endl;
	cin >> opt;
	
	if (opt == 1){//AGREGAR DATOS
		for (int i = 0; i < max; i++){
			cin >> dato;
			p.push(dato);
		}
	}
	
	else if (opt == 2){//ELIMINAR DATO TOPE
		
	}
	
	else if (opt == 3){//VER PILA
		p.muestra_pila();
	}
	
	else if (opt == 4){//SALIR
		resp = 0;
	} 
	
	else{
		cout << "Opcion invalida...";
	}
}

return 0;
}

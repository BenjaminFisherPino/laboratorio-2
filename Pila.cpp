#include <iostream>
#include <ctype.h>
using namespace std;

#include "Pila.h"

//CONSTRUCTOR CON UN PARAMETRO
Pila::Pila(int max){
	this->max = max;
	this->pila = new int[max];
	this->band = false;
	this->tope = 0;
	}

void Pila::pila_vacia(int tope, bool band){
	if (tope == 0){
		this->band = true; //Pila vacia
	}

	else{
		this->band = false; //La pila no esta vacia
	}
}

void Pila::pila_llena(){
	if (tope == max){
		this->band = true;
	}
	
	else{
		this->band = false;
	}
}

void Pila::push(int dato){
	if (band){
		cout << "Desbordamiento, pila llena...";
	}
	
	else{
		this->pila[tope] = dato;
		this->tope++;
	}
}

void Pila::pop(){
	if (band){
		//cout << "Se elimino el numero " << pila[tope] << " de la pila..." << endl;
		//BUSCAR COMO ELIMINAR DE LA PILA!!
		this->tope = tope - 1;
	}
	
	else{
		cout << "La pila esta vacia..." << endl;
	}
}

void Pila::muestra_pila(){
	for (int i = 0 ; i < this->max ; i++){
		if (this->tope > 0){
			cout << "[" << this->pila[tope] << "]" << endl;
			this->tope = tope - 1;
		}
		else{
			
		}
	}
}

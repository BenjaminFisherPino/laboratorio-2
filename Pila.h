#ifndef PILA_H
#define PILA_H

class Pila {

    //ATRIBUTOS
    private:
		int max;
		int* pila;
		int tope;
		bool band;
    public:
    //CONSTRUCTOR CON UN PARAMETRO
		Pila(int max);

    //METODOS
		
		//Seters
		void pila_vacia(int tope, bool band);
		void pila_llena();
		void push(int dato);
		void pop();
		void muestra_pila();
};
#endif

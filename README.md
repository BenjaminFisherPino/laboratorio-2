# Archivo README.md

**Versión 2.0**

Archivo README.md para laboratorio numero 2 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 12/09/2021

---
## Resúmen del programa

Este programa fue desarrollado por Benjmaín Fisher para poder almacenar, eliminar y revisar datos en una estructura de tipo pila.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

---
## Como instalarlo

Para instalar el programa debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/guia-c-plus-plus/-/edit/master/README.md

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi usted debe escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

El programa funciona a travez de una funcion Main, la cual permite que el compilador pueda construir y ejecutar el archivo. En esta funcion Main se instancia la creacion de un objeto de tipo Pila, la cual nos sirve como mediador
entre el usuario y el computador. En esta se almacena y se accesan a los datos ingresados por teclado.

Espero que este archivo README.md les haya ayudado a resolver sus dudas generales respecto al programa. En caso de tener más dudas la direccion de correo electroncio al que se pueden dirigir es bfisher20@alumnos.utalca.cl.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca
